#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
|------------------------------------------------------------------------------------------------------------------------------
| TOKENS DA LINGUAGEM
|------------------------------------------------------------------------------------------------------------------------------
*/

#define  TOKEN_CLASS                  "<class>"
#define  TOKEN_EXTEND                 "<extends>"
#define  TOKEN_PUBLIC                 "<public>"
#define  TOKEN_STATIC                 "<static>"
#define  TOKEN_VOID                   "<void>"
#define  TOKEN_MAIN                   "<main>"
#define  TOKEN_STRING                 "<String>"
#define  TOKEN_SYSTEM_OUT             "<System.out.println>"
#define  TOKEN_RETURN                 "<return>"
#define  TOKEN_INT                    "<int>"
#define  TOKEN_ATRIBUICAO             "<atribuicao>"
#define  TOKEN_BOOLEAN                "<boolean>"
#define  TOKEN_TRUE                   "<true>"
#define  TOKEN_FALSE                  "<false>"
#define  TOKEN_THIS                   "<this>"
#define  TOKEN_NULL                   "<null>"
#define  TOKEN_LENGTH                 "<length>"
#define  TOKEN_ELSE                   "<else>"
#define  TOKEN_NEW                    "<new>"
#define  TOKEN_IF                     "<if>"
#define  TOKEN_WHILE                  "<while>"
#define  TOKEN_ID                     "<id,"
#define  TOKEN_NUMERO                 "<numero,"
#define  TOKEN_COMP_MENOR             "<comparacao,MENOR>"
#define  TOKEN_COMP_IGUAL             "<comparacao,IGUAL>"
#define  TOKEN_COMP_DIFERENTE         "<comparacao,DIFERENTE>"
#define  TOKEN_OP_LOG_NOT             "<oplogico,NOT>"
#define  TOKEN_OP_LOG_AND             "<oplogico,AND>"
#define  TOKEN_OP_ARIT_SOMA           "<oparitmetico,SOMA>"
#define  TOKEN_OP_ARIT_SUBTRACAO      "<oparitmetico,SUBTRACAO>"
#define  TOKEN_OP_ARIT_MULTIPLICACAO  "<oparitmetico,MULTIPLICACAO>"
#define  TOKEN_PONT_APARENTESES       "<pontuacao,APARENTESES>"
#define  TOKEN_PONT_FPARENTESES       "<pontuacao,FPARENTESES>"
#define  TOKEN_PONT_ACHAVES           "<pontuacao,ACHAVES>"
#define  TOKEN_PONT_FCHAVES           "<pontuacao,FCHAVES>"
#define  TOKEN_PONT_ACOLCHETES        "<pontuacao,ACOLCHETES>"
#define  TOKEN_PONT_FCOLCHETES        "<pontuacao,FCOLCHETES>"
#define  TOKEN_PONT_PONTOVIRGULA      "<pontuacao,PONTOVIRGULA>"
#define  TOKEN_PONT_PONTO             "<pontuacao,PONTO>"
#define  TOKEN_PONT_VIRGULA           "<pontuacao,VIRGULA>"
#define  TOKEN_COMENT_LINHA           "<comentario,LINHA>"
#define  TOKEN_COMENT_MULTI_INICIO    "<comentario,MULTINICIO>"
#define  TOKEN_COMENT_MULTI_FINAL     "<comentario,MULTFINAL>"

/*
|------------------------------------------------------------------------------------------------------------------------------
| CONSTANTES E ESTRUTURAS DE DADOS PARA A TABELA DE SÍMBOLOS
|------------------------------------------------------------------------------------------------------------------------------
*/

#define TS_TIPO_ID_VARIAVEL 1
#define TS_TIPO_ID_CLASSE 2
#define TS_TIPO_ID_METODO 3
#define TS_TIPO_ID_ATRIBUTO 4

typedef struct TID{
	int numero;   		    // Número de identificação na tabela de símbolos
	char *valor;  		    // Nome do variável, função, atributo, metodo, classe etc 
	int declarado;		    // 1 para sim. 0 para não
	int tipo_identificador; // 1, se variável. 0, se função, atributo etc
	char *tipo;	  		    // tipo da variavel. Tipo de retorno caso de método
	struct TID *proximo;    // Próxima referência na tabela de símbolos
} TID;

typedef struct TTabSimbolos{
	int nextID;
	TID *lista;
} TTabSimbolos;

/*
|------------------------------------------------------------------------------------------------------------------------------
| ESTRUTURAS DE DADOS E FUNÇÕES PARA ANÁLISE LÉXICA
|------------------------------------------------------------------------------------------------------------------------------
*/

typedef struct TLexema{
	char *valor;
	unsigned long tamanho;
	struct TLexema *proximo;
} TLexema;

int analisar_lexema(TLexema *lexema, FILE *output, TTabSimbolos *ts);

/*
|------------------------------------------------------------------------------------------------------------------------------
| ESTRUTURAS DE DADOS E FUNÇÕES PARA ANÁLISE SINTÁTICA
|------------------------------------------------------------------------------------------------------------------------------
*/

typedef struct TToken{
	char *valor;
	unsigned long tamanho;
	struct TToken *proximo;
} TToken;

typedef struct TTokens{
	TToken *inicio;
	TToken *final;
} TTokens;

int analise_sintatica(TToken *inicio);
void mostrar_erro_sintatico(char *token_inesperado, char *ultimo_token);
void* analisar_bloco_classe(TToken *anterior, TToken *atual);
void* analisar_declaracao_metodo(TToken *anterior, TToken *atual);
void* analisar_parametros(TToken *anterior, TToken *atual);
void* analisar_argumentos_chamada(TToken *anterior, TToken *atual);
void* analisar_indice_vetor(TToken *anterior, TToken *atual);
void* analisar_token_identificador(TToken *anterior, TToken *atual);
void* analisar_token_this(TToken *anterior, TToken *atual);
void* analisar_token_new(TToken *anterior, TToken *atual);
void* analisar_statement(TToken *anterior, TToken *atual);
void* analisar_bloco(TToken *anterior, TToken *atual);
void* analisar_declaracao_variaveis(TToken *anterior, TToken *atual, char *tipo);
void* analisar_expressao(TToken *anterior, TToken *atual, TID *lval);

/*
|------------------------------------------------------------------------------------------------------------------------------
| START DO COMPILADOR
|------------------------------------------------------------------------------------------------------------------------------
*/

TTabSimbolos *tabela;

int main(){
	FILE *input, *output;

	TLexema *inicio = NULL;
	TLexema *final  = NULL;
	TLexema *atual  = NULL;

	TTokens	*tokens = NULL;

	if((input = fopen("alexico.txt", "r"))){
		char buffer[100];
		int processando = 1;
		
		//Leitura do arquivo e montagem da lista de lexemas
		do{
			if(fscanf(input, "%s", buffer) > 0){
				TLexema *lexema = (TLexema *) calloc(1, sizeof(TLexema));

				lexema->tamanho = strlen(buffer);
				lexema->valor   = (char *) calloc(lexema->tamanho, sizeof(char));

				strcpy(lexema->valor, buffer);

				if(inicio){
					final->proximo = lexema;
					final = final->proximo;
				}
				else{
					inicio = lexema;
					final = lexema;
				}
			}
			else{
				processando = 0;
				fclose(input);
			}
		}while(processando);

		//Início da análise léxica
		if(inicio){
			atual = inicio;

			if((output = fopen("alexico_resultado.txt", "w"))){
				tabela = (TTabSimbolos *) calloc(1, sizeof(TTabSimbolos));
				tabela->nextID = 1;
				tabela->lista  = NULL;

				while(atual){
					
					if(!analisar_lexema(atual, output, tabela)){
						printf("ERRO LEXICO: '%s' nao reconhecido.\n", atual->valor);
						fclose(output);
						goto desalocarRecursos;
					}
					
					atual = atual->proximo;
				}

				fclose(output);
				printf("Analize lexica concluida.\n");
				

				if((input = fopen("alexico_resultado.txt", "r"))){
					tokens = (TTokens *) calloc(1, sizeof(TTokens));
					processando = 1;

					//Leitura do arquivo e montagem da lista de tokens	
					do{
						if(fscanf(input, "%s", buffer) > 0){
							TToken *token = (TToken *) calloc(1, sizeof(TToken));
							token->tamanho = strlen(buffer);
							token->valor   = (char *) calloc(token->tamanho, sizeof(char));
							
							strcpy(token->valor, buffer);

							if(tokens->inicio){
								tokens->final->proximo = token;
								tokens->final = tokens->final->proximo;
							}
							else{
								tokens->inicio = token;
								tokens->final = token;
							}
						}
						else{
							processando = 0;
							fclose(input);
						}
					}while(processando);

					//Início da análise sintática
					if(tokens->inicio){
						if(analise_sintatica(tokens->inicio) == 0){
							goto desalocarRecursos;
						}
					}

					printf("Analize sintatica concluida.\nAnalise semantica concluida.\n");
				}
				else{
					printf("Nao foi possivel ler o arquivo de tokens.\n");
				}
			}
			else{
				printf("Nao foi possivel criar o arquivo de tokens.\n");
			}
		}

		
		desalocarRecursos:

		if(tokens){
			while(tokens->inicio){
				TToken *taux = tokens->inicio;
				tokens->inicio = taux->proximo;
				free(taux);
			}

			free(tokens);
		}

		if(tabela){

			/*//Para debug da tabela de simbolos - TITULOS
			printf("\n%5s | %15s | %10s | %7s | %10s \n", "ID", "NOME", "DECLARADO?", "TIPO ID", "TIPO");*/

			while(tabela->lista){
				TID *aux = tabela->lista;

				/*//Para debug da tabela de simbolos - DADOS
				printf("%5d | %15s | %10d | %7d | %10s \n", aux->numero, aux->valor, aux->declarado, aux->tipo_identificador, aux->tipo);*/

				tabela->lista = aux->proximo;
				free(aux);
			}

			free(tabela);
		}

		while(inicio){
			atual  = inicio;
			inicio = inicio->proximo;
			free(atual->valor);
			free(atual);
		}
	}
	else{
		printf("Nao foi possivel abrir o arquivo.\n");
	}


	#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
	    system("pause");
	#endif
	
	return 0;
}

/*
|------------------------------------------------------------------------------------------------------------------------------
| IMPLEMETAÇÃO DE FUNÇÕES PARA ANÁLISE LÉXICA
|------------------------------------------------------------------------------------------------------------------------------
*/

int analisar_lexema(TLexema *lexema, FILE *output, TTabSimbolos *ts){
	int index = 0;

	qi: 
		switch(lexema->valor[index]){
			case 'b': goto q0;
			case 'c': goto q7;
			case 'e': goto q12;
			case 'p': goto q19;
			case 's': goto q25;
			case 'v': goto q31;
			case 'm': goto q35;
			case 'S': goto q39;
			case 'r': goto q62;
			case 'i': goto q68;
			case '(': goto q71;
			case ')': goto q73;
			case '{': goto q74;
			case '}': goto q75;
			case '[': goto q76;
			case ']': goto q77;
			case ';': goto q78;
			case '.': goto q79;
			case ',': goto q80;
			case '=': goto q81;
			case '<': goto q82;
			case '!': goto q84;
			case '+': goto q86;
			case '-': goto q87;
			case '*': goto q88;
			case '&': goto q89;
			case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': goto q91;
			case '/': goto q95;
			case 'f': goto q93;
			case 't': goto q102;
			case 'n': goto q109;
			case 'l': goto q113;
			case 'w': goto q125;
			case 'a': case 'd': case 'g': case 'h': case 'j': case 'k': case 'o': case 'q': case 'u': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': goto q92;
		}

		goto qerro;
	
	
	//-------------------------------------------------------------------------------------------------------------------
	q0:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'o': goto q1;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
		
	//-------------------------------------------------------------------------------------------------------------------
	q1:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'o': goto q2;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q2:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q3;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q3:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q4;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q4:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': goto q5;
			case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q5:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q6;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q6:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_BOOLEAN);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q7:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q8;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q8:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': goto q9;
			case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q9:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 's': goto q10;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q10:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 's': goto q11;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q11:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_CLASS);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q12:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'x': goto q13;
			case 'l': goto q119;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q13:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q14;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q14:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q15;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q15:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q16;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q16:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'd': goto q17;
			case 'a': case 'b': case 'c': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q17:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 's': goto q18;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q18:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_EXTEND);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q19:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'u': goto q20;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q20:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'b': goto q21;
			case 'a': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q21:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q22;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q22:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q23;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q23:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'c': goto q24;
			case 'a': case 'b': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q24:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PUBLIC);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q25:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q26;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q26:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': goto q27;
			case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q27:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q28;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q28:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q29;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q29:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'c': goto q30;
			case 'a': case 'b': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q30:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_STATIC);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q31:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'o': goto q32;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q32:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q33;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q33:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'd': goto q34;
			case 'a': case 'b': case 'c': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q34:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_VOID);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q35:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': goto q36;
			case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q36:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q37;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q37:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q38;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q38:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_MAIN);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q39:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q40;
			case 'y': goto q45;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q40:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'r': goto q41;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q41:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q42;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q42:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q43;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q43:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'g': goto q44;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q44:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_STRING);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q45:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 's': goto q46;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q46:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q47;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q47:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q48;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q48:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'm': goto q49;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q49:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case '.': goto q50;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q50:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'o': goto q51;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q51:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'u': goto q52;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q52:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q53;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q53:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case '.': goto q54;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q54:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'p': goto q55;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q55:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'r': goto q56;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;	
	
	//-------------------------------------------------------------------------------------------------------------------
	q56:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q57;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q57:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q58;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q58:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q59;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q59:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q60;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q60:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q61;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q61:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_SYSTEM_OUT);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q62:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q63;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q63:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q64;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q64:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'u': goto q65;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q65:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'r': goto q66;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q66:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q67;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q67:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_RETURN);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q68:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q69;
			case 'f': goto q124;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q69:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q70;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q70:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_INT);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q71:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_APARENTESES);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q73:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_FPARENTESES);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q74:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_ACHAVES);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q75:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_FCHAVES);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q76:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_ACOLCHETES);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q77:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_FCOLCHETES);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q78:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_PONTOVIRGULA);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q79:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_PONTO);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q80:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_PONT_VIRGULA);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q81:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_ATRIBUICAO);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case '=': goto q83;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q82:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_COMP_MENOR);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q83:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_COMP_IGUAL);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q84:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_OP_LOG_NOT);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case '=': goto q85;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q85:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_COMP_DIFERENTE);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q86:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_OP_ARIT_SOMA);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q87:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_OP_ARIT_SUBTRACAO);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q88:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_OP_ARIT_MULTIPLICACAO);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case '/': goto q98;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q89:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case '&': goto q90;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q90:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_OP_LOG_AND);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q91:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s%s>\n", TOKEN_NUMERO, lexema->valor);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': goto q91;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q93:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': goto q94;
			case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q94:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q99;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q95:
		if(index == lexema->tamanho - 1){
			goto qerro;
		}

		++index;

		switch(lexema->valor[index]){
			case '/': goto q96;
			case '*': goto q97;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q96:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_COMENT_LINHA);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q97:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_COMENT_MULTI_INICIO);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q98:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_COMENT_MULTI_FINAL);
			goto qsucesso;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q99:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 's': goto q100;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q100:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q101;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q101:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_FALSE);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q102:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'h': goto q103;
			case 'r': goto q106;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q103:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q104;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q104:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 's': goto q105;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q105:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_THIS);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q106:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'u': goto q107;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q107:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q108;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q108:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_TRUE);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q109:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'u': goto q110;
			case 'e': goto q122;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q110:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q111;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q111:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q112;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q112:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_NULL);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q113:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q114;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q114:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'n': goto q115;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q115:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'g': goto q116;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q116:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 't': goto q117;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q117:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'h': goto q118;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q118:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_LENGTH);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q119:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 's': goto q120;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q120:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q121;
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q121:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_ELSE);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q122:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'w': goto q123;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;
	
	//-------------------------------------------------------------------------------------------------------------------
	q123:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_NEW);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;


	//-------------------------------------------------------------------------------------------------------------------
	q124:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_IF);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){ 
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;

	//-------------------------------------------------------------------------------------------------------------------
	q125:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'h': goto q126; 
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;

	//-------------------------------------------------------------------------------------------------------------------
	q126:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'i': goto q127;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;

	//-------------------------------------------------------------------------------------------------------------------
	q127:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'l': goto q128;
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;

	//-------------------------------------------------------------------------------------------------------------------
	q128:
		if(index == lexema->tamanho - 1){
			goto q92;
		}

		++index;

		switch(lexema->valor[index]){
			case 'e': goto q129; 
			case 'a': case 'b': case 'c': case 'd': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;

	//-------------------------------------------------------------------------------------------------------------------
	q129:
		if(index == lexema->tamanho - 1){
			fprintf(output, "%s\n", TOKEN_WHILE);
			goto qsucesso;
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;

	//-------------------------------------------------------------------------------------------------------------------
	q92:
		if(index == lexema->tamanho - 1){
			if(ts->lista){
				TID *aux = ts->lista;

				while(aux){

					if(strcmp(aux->valor, lexema->valor) == 0){
						fprintf(output, "%s%d>\n", TOKEN_ID, aux->numero);
						goto qsucesso;
					}

					if(aux->proximo){
						aux = aux->proximo;
					}
					else{
						TID *id = (TID *) calloc(1, sizeof(TID));
						id->numero = ts->nextID;
						id->valor  = lexema->valor;

						aux->proximo = id;

						++ts->nextID;

						fprintf(output, "%s%d>\n", TOKEN_ID, id->numero);
						goto qsucesso;
					}
				}
			}
			else{
				TID *id = (TID *) calloc(1, sizeof(TID));
				
				id->numero = ts->nextID;
				id->valor  = lexema->valor;

				++ts->nextID;

				ts->lista = id;

				fprintf(output, "%s%d>\n", TOKEN_ID, id->numero);
				goto qsucesso;
			}
		}

		++index;

		switch(lexema->valor[index]){
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '_': goto q92;
		}

		goto qerro;

	qerro: 
		return 0;

	qsucesso: 
		return 1;
}

/*
|------------------------------------------------------------------------------------------------------------------------------
| IMPLEMETAÇÃO DE FUNÇÕES PARA ANÁLISE SEMANTICA
|------------------------------------------------------------------------------------------------------------------------------
*/

int set_dados_identificador(char *token, int declarado, int tipo_identificador, char *tipo){
	int id;
	sscanf(token, "<id,%d>", &id);

	TID *aux = tabela->lista;

	while(aux){
		if(aux->numero == id){
			if(aux->declarado == 1){
				printf("ERRO SEMANTICO: tentando redeclarar o identificador %s.\n", aux->valor);
				break;
			}
			else{
				aux->declarado          = declarado;
				aux->tipo_identificador = tipo_identificador;
				aux->tipo               = tipo;

				return 1;
			}
		}

		aux = aux->proximo;
	}

	return 0;
}

TID* get_dados_identificador(char *token){
	int id;
	sscanf(token, "<id,%d>", &id);

	TID *aux = tabela->lista;

	while(aux){
		if(aux->numero == id){	
			return aux;
		}

		aux = aux->proximo;
	}

	return NULL;
}

/*
|------------------------------------------------------------------------------------------------------------------------------
| IMPLEMETAÇÃO DE FUNÇÕES PARA ANÁLISE SINTÁTICA
|------------------------------------------------------------------------------------------------------------------------------
*/

int analise_sintatica(TToken *inicio){
	TToken *atual    = inicio;
	TToken *anterior = atual;

	while(atual){
		if(strcmp(atual->valor, TOKEN_CLASS) == 0){
			anterior = atual;
			atual    = atual->proximo;

			if(strstr(atual->valor, TOKEN_ID)){

				TID *identificador = get_dados_identificador(atual->valor);

				if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_CLASSE, identificador->valor) == 0){
					goto erro;
				}

				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_PONT_ACHAVES) == 0){
					anterior = atual;
					atual = atual->proximo;

					if((atual = analisar_bloco_classe(anterior, atual)) == NULL){
						printf("Erro ao analisar o conteudo da classe.\n");
						goto erro;
					}

					goto proximaInstrucao;
				}
				else if(strcmp(atual->valor, TOKEN_EXTEND) == 0){
					anterior = atual;
					atual = atual->proximo;

					if(strstr(atual->valor, TOKEN_ID)){
						anterior = atual;
						atual = atual->proximo;

						if(strcmp(atual->valor, TOKEN_PONT_ACHAVES) == 0){
							anterior = atual;
							atual = atual->proximo;

							if((atual = analisar_bloco_classe(anterior, atual)) == NULL){
								printf("Erro ao analisar o conteudo da classe.\n");
								goto erro;
							}

							goto proximaInstrucao;
						}
					}
				}
			}

			goto erroSintatico;
		}
		else{
			printf("ERRO SINTATICO: token '%s' inesperado na raiz do programa fonte. Era esperado um token %s.\n", atual->valor, TOKEN_CLASS);
			goto erro;
		}

		proximaInstrucao:
			anterior = atual;
			atual = atual->proximo;
	}

	return 1;

	erroSintatico:
		mostrar_erro_sintatico(atual->valor, anterior->valor);

	erro:
		return 0;
}

void mostrar_erro_sintatico(char *token_inesperado, char *ultimo_token){
	char *nomeIdInesperado = ""; 
	char *nomeUltimoId = "";

	if(strstr(token_inesperado, TOKEN_ID)){
		int id;
		sscanf(token_inesperado, "<id,%d>", &id);

		TID *aux = tabela->lista;

		while(aux){
			if(aux->numero == (int) id){
				nomeIdInesperado = aux->valor;
			}

			aux = aux->proximo;
		}
	}

	if(strstr(ultimo_token, TOKEN_ID)){
		int id;
		sscanf(ultimo_token, "<id,%d>", &id);

		TID *aux = tabela->lista;

		while(aux){
			if(aux->numero == (int) id){
				nomeUltimoId = aux->valor;
			}

			aux = aux->proximo;
		}
	}

	if(strlen(nomeIdInesperado) > 0 && strlen(nomeUltimoId) > 0){
		printf("ERRO SINTATICO: Inesperado token '%s (%s)'  apos '%s (%s)'.\n", token_inesperado, nomeIdInesperado, ultimo_token, nomeUltimoId);
	}
	else if(strlen(nomeIdInesperado) > 0){
		printf("ERRO SINTATICO: Inesperado token '%s (%s)' apos '%s'.\n", token_inesperado, nomeIdInesperado, ultimo_token);
	}
	else if(strlen(nomeUltimoId) > 0){
		printf("ERRO SINTATICO: Inesperado token '%s' apos '%s (%s)'.\n", token_inesperado, ultimo_token, nomeUltimoId);
	}
	else{
		printf("ERRO SINTATICO: Inesperado token '%s' apos '%s'.\n", token_inesperado, ultimo_token);
	}
}

void* analisar_bloco_classe(TToken *anterior, TToken *atual){
	if(strcmp(atual->valor, TOKEN_PUBLIC) == 0){
		do{
			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_STATIC) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_VOID) == 0){
					anterior = atual;
					atual = atual->proximo;
					
					if(strcmp(atual->valor, TOKEN_MAIN) == 0){
						anterior = atual;
						atual = atual->proximo;
						
						if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
							anterior = atual;
							atual = atual->proximo;
							
							if(strcmp(atual->valor, TOKEN_STRING) == 0){
								anterior = atual;
								atual = atual->proximo;
								
								if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
									anterior = atual;
									atual = atual->proximo;
									
									if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
										anterior = atual;
										atual = atual->proximo;
										
										if(strstr(atual->valor, TOKEN_ID)){
											if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_VARIAVEL, "String[]") == 0){
												goto erroBlocoClasse;
											}

											anterior = atual;
											atual = atual->proximo;
											
											if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
												anterior = atual;
												atual = atual->proximo;
												
												if(strcmp(atual->valor, TOKEN_PONT_ACHAVES) == 0){
													anterior = atual;
													atual = atual->proximo;

													if((atual = analisar_bloco(anterior, atual)) == NULL){
														printf("Erro ao analisar o conteudo do método main.\n");
														goto erroBlocoClasse;
													}

													goto proximaInstrucao;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else if(strcmp(atual->valor, TOKEN_VOID) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strstr(atual->valor, TOKEN_ID)){
					
					if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_METODO, "void") == 0){
						goto erroBlocoClasse;
					}

					anterior = atual;
					atual = atual->proximo;

					if((atual = analisar_declaracao_metodo(anterior, atual)) == NULL){
						printf("Erro na declaracao do metodo.\n");
						goto erroBlocoClasse;
					}

					goto proximaInstrucao;
				}
			}
			else if(strcmp(atual->valor, TOKEN_BOOLEAN) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strstr(atual->valor, TOKEN_ID)){
					anterior = atual;
					atual = atual->proximo;

					if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){

						if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_ATRIBUTO, "boolean") == 0){
							goto erroBlocoClasse;
						}

						goto proximaInstrucao;
					}

					if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_METODO, "boolean") == 0){
						goto erroBlocoClasse;
					}

					if((atual = analisar_declaracao_metodo(anterior, atual)) == NULL){
						printf("Erro na declaracao do metodo.\n");
						goto erroBlocoClasse;
					}

					goto proximaInstrucao;
				}
			}
			else if(strcmp(atual->valor, TOKEN_INT) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strstr(atual->valor, TOKEN_ID)){
					anterior = atual;
					atual = atual->proximo;

					if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){

						if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_ATRIBUTO, "int") == 0){
							goto erroBlocoClasse;
						}

						goto proximaInstrucao;
					}

					if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_METODO, "int") == 0){
						goto erroBlocoClasse;
					}
					
					if((atual = analisar_declaracao_metodo(anterior, atual)) == NULL){
						printf("Erro na declaracao do metodo.\n");
						goto erroBlocoClasse;
					}

					goto proximaInstrucao;
				}
				else if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
					anterior = atual;
					atual = atual->proximo;
					
					if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
						anterior = atual;
						atual = atual->proximo;
						
						if(strstr(atual->valor, TOKEN_ID)){
							anterior = atual;
							atual = atual->proximo;

							if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){

								if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_ATRIBUTO, "int[]") == 0){
									goto erroBlocoClasse;
								}

								goto proximaInstrucao;
							}
							
							if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_METODO, "int[]") == 0){
								goto erroBlocoClasse;
							}

							if((atual = analisar_declaracao_metodo(anterior, atual)) == NULL){
								printf("Erro na declaracao do metodo.\n");
								goto erroBlocoClasse;
							}

							goto proximaInstrucao;
						}
					}
				}
			}
			else if(strstr(atual->valor, TOKEN_ID)){

				anterior = atual;
				atual = atual->proximo;

				if(strstr(atual->valor, TOKEN_ID)){
					TID *identificador = get_dados_identificador(anterior->valor);

					anterior = atual;
					atual = atual->proximo;

					if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){

						if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_ATRIBUTO, identificador->valor) == 0){
							goto erroBlocoClasse;
						}

						goto proximaInstrucao;
					}

					if(set_dados_identificador(anterior->valor, 1, TS_TIPO_ID_METODO, identificador->valor) == 0){
						goto erroBlocoClasse;
					}
					
					if((atual = analisar_declaracao_metodo(anterior, atual)) == NULL){
						printf("Erro na declaracao do metodo.\n");
						goto erroBlocoClasse;
					}

					goto proximaInstrucao;
				}
			}
			
			goto erroSintaticoBlocoClasse;

			proximaInstrucao:
				anterior = atual;
				atual = atual->proximo;

		}while(strcmp(atual->valor, TOKEN_PUBLIC) == 0);
	}
	

	if(strcmp(atual->valor, TOKEN_PONT_FCHAVES) == 0){
		return atual;
	}

	erroSintaticoBlocoClasse:
		mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroBlocoClasse:
		return NULL;
}

void* analisar_declaracao_metodo(TToken *anterior, TToken *atual){
	if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_parametros(anterior, atual)) == NULL){
			printf("Erro ao analisar os parametros do método.\n");
			goto erroDeclaracaoMetodo;
		}

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_ACHAVES) == 0){
			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_bloco(anterior, atual)) == NULL){
				printf("Erro ao analisar o conteudo do método.\n");
				goto erroDeclaracaoMetodo;
			}

			return atual;
		}
	}

	mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroDeclaracaoMetodo:
		return NULL;
}

void* analisar_parametros(TToken *anterior, TToken *atual){
	if(strcmp(atual->valor, TOKEN_BOOLEAN) == 0){
		anterior = atual;
		atual = atual->proximo;

		if(strstr(atual->valor, TOKEN_ID)){

			if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_VARIAVEL, "boolean") == 0){
				goto erroParametros;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_VIRGULA) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) != 0){
					if((atual = analisar_parametros(anterior, atual)) == NULL){
						goto erroParametros;
					}

					return atual;
				}

				goto erroSintaticoParametros;
			}

			goto testarFim;
		}

		goto erroSintaticoParametros;
	}
	else if(strcmp(atual->valor, TOKEN_INT) == 0){
		anterior = atual;
		atual = atual->proximo;

		if(strstr(atual->valor, TOKEN_ID)){

			if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_VARIAVEL, "int") == 0){
				goto erroParametros;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_VIRGULA) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) != 0){
					if((atual = analisar_parametros(anterior, atual)) == NULL){
						goto erroParametros;
					}

					return atual;
				}

				goto erroSintaticoParametros;
			}

			goto testarFim;
		}
		else if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
			anterior = atual;
			atual = atual->proximo;
			
			if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
				anterior = atual;
				atual = atual->proximo;
				
				if(strstr(atual->valor, TOKEN_ID)){

					if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_VARIAVEL, "int[]") == 0){
						goto erroParametros;
					}

					anterior = atual;
					atual = atual->proximo;

					if(strcmp(atual->valor, TOKEN_PONT_VIRGULA) == 0){
						anterior = atual;
						atual = atual->proximo;

						if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) != 0){
							if((atual = analisar_parametros(anterior, atual)) == NULL){
								goto erroParametros;
							}

							return atual;
						}

						goto erroSintaticoParametros;
					}

					goto testarFim;
				}
			}
		}

		goto erroSintaticoParametros;
	}
	else if(strstr(atual->valor, TOKEN_ID)){
		anterior = atual;
		atual = atual->proximo;

		if(strstr(atual->valor, TOKEN_ID)){

			TID *identificador = get_dados_identificador(anterior->valor);

			if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_VARIAVEL, identificador->valor) == 0){
				goto erroParametros;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_VIRGULA) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) != 0){
					if((atual = analisar_parametros(anterior, atual)) == NULL){
						goto erroParametros;
					}

					return atual;
				}

				goto erroSintaticoParametros;
			}

			goto testarFim;
		}

		goto erroSintaticoParametros;
	}

	testarFim:
		if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
			return atual;
		}

	erroSintaticoParametros:
		mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroParametros:
		return NULL;
}

void* analisar_argumentos_chamada(TToken *anterior, TToken *atual){
	if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
		goto fimDosArgumentosDaChamada;
	}
	else{
		proximoArgumentoDaChamada:

		if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
			printf("Erro ao analisar os argumentos da chamada de metodo.\n");
			goto erroAnaliseArgumentos;
		}

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_VIRGULA) == 0){
			anterior = atual;
			atual = atual->proximo;

			goto proximoArgumentoDaChamada;
		}
		else if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
			goto fimDosArgumentosDaChamada;
		}
	}
	
	mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroAnaliseArgumentos:
		return NULL;

	fimDosArgumentosDaChamada:
		return atual;
}

void* analisar_indice_vetor(TToken *anterior, TToken *atual){
	if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
		goto erroAnaliseIndiveVetor;
	}

	anterior = atual;
	atual = atual->proximo;

	if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
		return atual;
	}

	erroAnaliseIndiveVetor:
		return NULL;
}

void* analisar_token_identificador(TToken *anterior, TToken *atual){
	inicioDaAnaliseIdentificador:
	
	if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_argumentos_chamada(anterior, atual)) == NULL){
			goto erroAnaliseIdentificador;
		}

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_indice_vetor(anterior, atual)) == NULL){
				goto erroAnaliseIdentificador;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_PONTO) == 0){
				goto analisarIdentificadorAposPonto;
			}
		}
		else if(strcmp(atual->valor, TOKEN_PONT_PONTO) == 0){
			goto analisarIdentificadorAposPonto;
		}

		goto fimDaAnaliseIdentificador;
	}
	else if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_indice_vetor(anterior, atual)) == NULL){
			goto erroAnaliseIdentificador;
		}

		anterior = atual;
		atual = atual->proximo;
		
		if(strcmp(atual->valor, TOKEN_PONT_PONTO) == 0){
			goto analisarIdentificadorAposPonto;	
		}

		goto fimDaAnaliseIdentificador;
	}
	else if(strcmp(atual->valor, TOKEN_PONT_PONTO) == 0){
		analisarIdentificadorAposPonto:

		anterior = atual;
		atual = atual->proximo;
		
		if(strstr(atual->valor, TOKEN_ID)){
			anterior = atual;
			atual = atual->proximo;

			goto inicioDaAnaliseIdentificador;
		}
		else if(strcmp(atual->valor, TOKEN_LENGTH) == 0){
			anterior = atual;
			atual = atual->proximo;

			goto fimDaAnaliseIdentificador;
		}
	}
	else{
		goto fimDaAnaliseIdentificador;
	}

	mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroAnaliseIdentificador:
		return NULL;

	fimDaAnaliseIdentificador:
		return anterior;
}


void* analisar_token_this(TToken *anterior, TToken *atual){
	return analisar_token_identificador(anterior, atual);
}

void* analisar_token_new(TToken *anterior, TToken *atual){
	if(strcmp(atual->valor, TOKEN_INT) == 0){
		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
				goto erroAnaliseTokenNew;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
				anterior = atual;
				atual = atual->proximo;

				goto fimAnaliseTokenNew;
			}
		}
	}
	else if(strstr(atual->valor, TOKEN_ID)){
		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_argumentos_chamada(anterior, atual)) == NULL){
				goto erroAnaliseTokenNew;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_PONTO) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strstr(atual->valor, TOKEN_ID)){
					anterior = atual;
					atual = atual->proximo;

					return analisar_token_identificador(anterior, atual);
				}
				else if(strcmp(atual->valor, TOKEN_LENGTH) == 0){
					anterior = atual;
					atual = atual->proximo;
				}
			}

			goto fimAnaliseTokenNew;
		}
	}

	mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroAnaliseTokenNew:
		return NULL;

	fimAnaliseTokenNew:
		return anterior;
}

void* analisar_statement(TToken *anterior, TToken *atual){
	if(strcmp(atual->valor, TOKEN_BOOLEAN) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_declaracao_variaveis(anterior, atual, "boolean")) == NULL){
			printf("Erro ao analisar a declaracao de variaveis.\n");
			goto erroStatement;
		}

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
			goto fimDoStatement;
		}
	}
	else if(strcmp(atual->valor, TOKEN_INT) == 0){
		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
			anterior = atual;
			atual = atual->proximo;
			
			if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
				anterior = atual;
				atual = atual->proximo;

				if((atual = analisar_declaracao_variaveis(anterior, atual, "int[]")) == NULL){
					printf("Erro ao analisar a declaracao de variaveis.\n");
					goto erroStatement;
				}

				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
					goto fimDoStatement;
				}
			}
		}
		else{
			if((atual = analisar_declaracao_variaveis(anterior, atual, "int")) == NULL){
				printf("Erro ao analisar a declaracao de variaveis.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
				goto fimDoStatement;
			}
		}
	}
	else if(strstr(atual->valor, TOKEN_ID)){
		anterior = atual;
		atual = atual->proximo;
		
		if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
			TID *identificador;
			
			atribuicaoIdentificador:

			identificador = get_dados_identificador(anterior->valor);

			if(identificador == NULL){
				printf("ERRO SEMANTICO: Identificador nao encontrado.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_expressao(anterior, atual, identificador)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
				goto fimDoStatement;
			}
		}
		else if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
					anterior = atual;
					atual = atual->proximo;

					if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
						printf("Erro ao analisar a expressao.\n");
						goto erroStatement;
					}

					anterior = atual;
					atual = atual->proximo;

					if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
						goto fimDoStatement;
					}
				}
			}
		}
		else if(strstr(atual->valor, TOKEN_ID)){
			TID *identificador = get_dados_identificador(anterior->valor);

			if((atual = analisar_declaracao_variaveis(anterior, atual, identificador->valor)) == NULL){
				printf("Erro ao analisar a declaracao de variaveis.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
				goto fimDoStatement;
			}
		}
		else{
			if((atual = analisar_token_identificador(anterior, atual)) == NULL){
				printf("Erro ao analisar o identificador.\n");
				goto erroStatement;
			}

			if(strstr(atual->valor, TOKEN_ID) || strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
					goto atribuicaoIdentificador;
				}
			}
			else if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
				anterior = atual;
				atual = atual->proximo;	

				if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
					goto fimDoStatement;
				}
			}
		}
	}
	else if(strcmp(atual->valor, TOKEN_THIS) == 0){
		anterior = atual;
		atual = atual->proximo;


		if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
			atribuicaoThis: 

			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
				goto fimDoStatement;
			}
		}
		else if(strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) == 0){
			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
					anterior = atual;
					atual = atual->proximo;

					if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
						printf("Erro ao analisar a expressao.\n");
						goto erroStatement;
					}

					anterior = atual;
					atual = atual->proximo;

					if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
						goto fimDoStatement;
					}
				}
			}
		}
		else{
			if((atual = analisar_token_this(anterior, atual)) == NULL){
				printf("Erro ao analisar o this.\n");
				goto erroStatement;
			}

			if(strstr(atual->valor, TOKEN_ID) || strcmp(atual->valor, TOKEN_PONT_FCOLCHETES) == 0){
				anterior = atual;
				atual = atual->proximo;

				if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
					goto atribuicaoThis;
				}
			}
			else if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
				anterior = atual;
				atual = atual->proximo;	

				if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
					goto fimDoStatement;
				}
			}
		}
	}
	else if(strcmp(atual->valor, TOKEN_NEW) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_token_new(anterior, atual)) == NULL){
			printf("Erro ao analisar a expressao.\n");
			goto erroStatement;
		}

		if(strstr(atual->valor, TOKEN_ID)){
			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
				goto atribuicaoIdentificador;
			}
		}
		else if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
			anterior = atual;
			atual = atual->proximo;	

			if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
				goto fimDoStatement;
			}
		}
	}
	else if(strcmp(atual->valor, TOKEN_SYSTEM_OUT) == 0){
		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
			anterior = atual;
			atual = atual->proximo;	

			if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
				anterior = atual;
				atual = atual->proximo;	

				if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
					goto fimDoStatement;
				}
			}
		}
	}
	else if(strcmp(atual->valor, TOKEN_RETURN) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
			printf("Erro ao analisar a expressao.\n");
			goto erroStatement;
		}

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
			goto fimDoStatement;
		}
	}
	else if(strcmp(atual->valor, TOKEN_IF) == 0){
		condicionalIf:

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
			anterior = atual;
			atual = atual->proximo;	

			if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
				anterior = atual;
				atual = atual->proximo;	

				if(strcmp(atual->valor, TOKEN_PONT_ACHAVES) == 0){
					anterior = atual;
					atual = atual->proximo;

					if((atual = analisar_bloco(anterior, atual)) == NULL){
						printf("Erro ao analisar o conteudo do bloco if.\n");
						goto erroStatement;
					}

					verificarElse:

					anterior = atual;
					atual = atual->proximo;

					if(strcmp(atual->valor, TOKEN_ELSE) == 0){
						anterior = atual;
						atual = atual->proximo;

						if(strcmp(atual->valor, TOKEN_PONT_ACHAVES) == 0){
							anterior = atual;
							atual = atual->proximo;

							if((atual = analisar_bloco(anterior, atual)) == NULL){
								printf("Erro ao analisar o conteudo do bloco if.\n");
								goto erroStatement;
							}
							
							goto fimDoStatement;
						}
						else if(strcmp(atual->valor, TOKEN_IF) == 0){
							goto condicionalIf;
						}
						else{
							if((atual = analisar_statement(anterior, atual)) == NULL){
								printf("Erro ao analisar o bloco de código.\n");
								goto erroStatement;
							}

							goto fimDoStatement;
						}
					}
					else{
						atual = anterior;
						goto fimDoStatement;
					}
				}
				else{
					if((atual = analisar_statement(anterior, atual)) == NULL){
						printf("Erro ao analisar o bloco de código.\n");
						goto erroStatement;
					}

					goto verificarElse;
				}
			}
		}
	}
	else if(strcmp(atual->valor, TOKEN_WHILE) == 0){
		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
			anterior = atual;
			atual = atual->proximo;	

			if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroStatement;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
				anterior = atual;
				atual = atual->proximo;	

				if(strcmp(atual->valor, TOKEN_PONT_PONTOVIRGULA) == 0){
					goto fimDoStatement;
				}
				else if(strcmp(atual->valor, TOKEN_PONT_ACHAVES) == 0){
					anterior = atual;
					atual = atual->proximo;

					if((atual = analisar_bloco(anterior, atual)) == NULL){
						printf("Erro ao analisar o conteudo do bloco while.\n");
						goto erroStatement;
					}

					goto fimDoStatement;
				}
				else{
					if((atual = analisar_statement(anterior, atual)) == NULL){
						printf("Erro ao analisar o bloco de código.\n");
						goto erroStatement;
					}

					goto fimDoStatement;
				}
			}
		}
	}

	erroSintaticoStatement:
		mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroStatement:
		return NULL;

	fimDoStatement:
		return atual;
}

void* analisar_bloco(TToken *anterior, TToken *atual){
	while(strcmp(atual->valor, TOKEN_PONT_FCHAVES) != 0){
		if((atual = analisar_statement(anterior, atual)) == NULL){
			printf("Erro ao analisar o bloco de código.\n");
			goto erroBloco;
		}

		anterior = atual;
		atual = atual->proximo;
	}

	return atual;
	
	erroSintaticoBloco:
		mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroBloco:
		return NULL;
}

void* analisar_declaracao_variaveis(TToken *anterior, TToken *atual, char *tipo){
	inicioDaDeclaracao:

	if(strstr(atual->valor, TOKEN_ID)){
		//grava tipo do identificador na tabela de símbolos

		if(set_dados_identificador(atual->valor, 1, TS_TIPO_ID_VARIAVEL, tipo) == 0){
			goto erroDeclaracaoVariaveis;
		}

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_VIRGULA) == 0){
			anterior = atual;
			atual = atual->proximo;

			goto inicioDaDeclaracao;
		}
		else if(strcmp(atual->valor, TOKEN_ATRIBUICAO) == 0){
			TID *rval = get_dados_identificador(anterior->valor);

			anterior = atual;
			atual = atual->proximo;

			if((atual = analisar_expressao(anterior, atual, rval)) == NULL){
				printf("Erro ao analisar a expressao.\n");
				goto erroDeclaracaoVariaveis;
			}

			anterior = atual;
			atual = atual->proximo;

			if(strcmp(atual->valor, TOKEN_PONT_VIRGULA) == 0){
				anterior = atual;
				atual = atual->proximo;

				goto inicioDaDeclaracao;
			}

			goto fimDeclaracaoVariaveis;
		}
		else{
			goto fimDeclaracaoVariaveis;	
		}
	}

	erroSintaticoDeclaracaoVariaveis:
		mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroDeclaracaoVariaveis:
		return NULL;

	fimDeclaracaoVariaveis:
		return anterior;
}

void* analisar_expressao(TToken *anterior, TToken *atual, TID *lval){
	inicioDaExpressao:

	if(strcmp(atual->valor, TOKEN_OP_LOG_NOT) == 0 || strcmp(atual->valor, TOKEN_OP_ARIT_SUBTRACAO) == 0){
		anterior = atual;
		atual = atual->proximo;
	}

	if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_expressao(anterior, atual, NULL)) == NULL){
			printf("Erro ao analisar a expressao.\n");
			goto erroExpressao;
		}

		anterior = atual;
		atual = atual->proximo;

		if(strcmp(atual->valor, TOKEN_PONT_FPARENTESES) == 0){
			anterior = atual;
			atual = atual->proximo;

			goto checarOperador;
		}
	}
	else if(strstr(atual->valor, TOKEN_NUMERO) || strcmp(atual->valor, TOKEN_TRUE) == 0 || strcmp(atual->valor, TOKEN_FALSE) == 0 || strcmp(atual->valor, TOKEN_NULL) == 0 ){
		
		if(lval && strstr(atual->valor, TOKEN_NUMERO) && strcmp(lval->tipo, "int") != 0){
			printf("ERRO SEMANTICO: 'numeros' nao podem ser atribuidos a '%s'\n", lval->tipo);
			goto erroExpressao;
		}

		if(lval && (strcmp(atual->valor, TOKEN_TRUE) == 0 || strcmp(atual->valor, TOKEN_FALSE) == 0) && strcmp(lval->tipo, "boolean") != 0){
			printf("ERRO SEMANTICO: O valor booleano '%s' nao podem ser atribuidos a '%s'\n", atual->valor, lval->tipo);
			goto erroExpressao;	
		}

		if(lval && strcmp(atual->valor, TOKEN_NULL) == 0 && (strcmp(lval->tipo, "boolean") == 0 || strcmp(lval->tipo, "int") == 0)){
			printf("ERRO SEMANTICO: NULL nao podem ser atribuidos a '%s'\n", lval->tipo);
			goto erroExpressao;	
		}

		anterior = atual;
		atual = atual->proximo;

		goto checarOperador;
	}
	else if(strstr(atual->valor, TOKEN_ID)){
		anterior = atual;
		atual = atual->proximo;

		TID *rval = get_dados_identificador(anterior->valor);

		if(strcmp(atual->valor, TOKEN_PONT_APARENTESES) != 0 && strcmp(atual->valor, TOKEN_PONT_ACOLCHETES) != 0 && strcmp(atual->valor, TOKEN_PONT_PONTO) != 0){
			if(lval){
				if(rval->declarado == 0){
					printf("ERRO SEMANTICO: Variável '%s' nao declarada\n", rval->valor);
					goto erroExpressao;	
				}
				else if(strcmp(lval->tipo, rval->tipo) != 0){
					printf("ERRO SEMANTICO: O valor do tipo '%s' nao podem ser atribuidos a '%s'\n", rval->tipo, lval->tipo);
					goto erroExpressao;	
				}
			}
		}

		if((atual = analisar_token_identificador(anterior, atual)) == NULL){
			printf("Erro ao analisar a expressao.\n");
			goto erroExpressao;
		}

		anterior = atual;
		atual = atual->proximo;

		goto checarOperador;
	}
	else if(strcmp(atual->valor, TOKEN_THIS) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_token_this(anterior, atual)) == NULL){
			printf("Erro ao analisar a expressao.\n");
			goto erroExpressao;
		}

		anterior = atual;
		atual = atual->proximo;

		goto checarOperador;
	}
	else if(strcmp(atual->valor, TOKEN_NEW) == 0){
		anterior = atual;
		atual = atual->proximo;

		if((atual = analisar_token_new(anterior, atual)) == NULL){
			printf("Erro ao analisar a expressao.\n");
			goto erroExpressao;
		}

		anterior = atual;
		atual = atual->proximo;

		goto checarOperador;
	}

	goto erroSintaticoExpressao;

	checarOperador:
		if(strcmp(atual->valor, TOKEN_OP_LOG_AND) == 0 || strcmp(atual->valor, TOKEN_COMP_MENOR) == 0 ||
			strcmp(atual->valor, TOKEN_COMP_IGUAL) == 0 || strcmp(atual->valor, TOKEN_COMP_DIFERENTE) == 0 ||
			strcmp(atual->valor, TOKEN_OP_ARIT_SOMA) == 0 || strcmp(atual->valor, TOKEN_OP_ARIT_SUBTRACAO) == 0 ||
			strcmp(atual->valor, TOKEN_OP_ARIT_MULTIPLICACAO) == 0){

			anterior = atual;
			atual = atual->proximo;

			goto inicioDaExpressao;
		}
		else{
			return anterior;
		}

	erroSintaticoExpressao:
		mostrar_erro_sintatico(atual->valor, anterior->valor);

	erroExpressao:
		return NULL;
}